-- ----------------------------------------------------------------------------
-- Need Primal Spirits
-- 		by Snipy
-- ----------------------------------------------------------------------------

NeedPrimalSpirits = {};
NeedPrimalSpiritsDB = NeedPrimalSpiritsDB or { ["need"] = true,["greed"] = false,["pass"] = false,["disable"] = false,};

function NeedPrimalSpirits.OnEvent(self,event,...)
	if NeedPrimalSpiritsDB and NeedPrimalSpiritsDB.disable then
		return;
	end
	if event == "START_LOOT_ROLL" then
		local id = ...;
		local _, name, _, quality, bindOnPickUp, canNeed, canGreed, canDisenchant = GetLootRollItemInfo(id);
		local primalSpiritName = select(1,GetItemInfo(120945));
		if name == primalSpiritName then
			-- Blizzard uses 0 to pass, 1 to Need an item, 2 to Greed an item, and 3 to Disenchant an item.
			-- Need > Greed > Pass
			if canNeed and NeedPrimalSpiritsDB.need then
				RollOnLoot(id, 1);
				StaticPopup_Hide("CONFIRM_LOOT_ROLL");
				C_Timer.After(.1,function()
						ConfirmLootRoll(id, 1);
						end);
			-- If loot is set to Need before Greed, needing primal spirits is disabled.
			elseif canGreed and (NeedPrimalSpiritsDB.greed or NeedPrimalSpiritsDB.need) then
				RollOnLoot(id, 2);
				StaticPopup_Hide("CONFIRM_LOOT_ROLL");
				C_Timer.After(.1,function()
						ConfirmLootRoll(id, 2);
						end);
			else
				RollOnLoot(id, 0);
				StaticPopup_Hide("CONFIRM_LOOT_ROLL");
				C_Timer.After(.1,function()
						ConfirmLootRoll(id, 0);
						end);
			end
		end
	end
end

------------------------------------------------
-- Slash Commands
------------------------------------------------
local function slashHandler(msg)
	msg = msg:lower() or "";
	if (msg == "disable") then
		NeedPrimalSpiritsDB.disable = true;
		NeedPrimalSpiritsDB.need = false;
		NeedPrimalSpiritsDB.greed = false;
		NeedPrimalSpiritsDB.pass = false;
		print("|cff33ff99NPS|r: Need Primal Spirits is disabled");
	elseif (msg == "need") then
		NeedPrimalSpiritsDB.disable = false;
		NeedPrimalSpiritsDB.need = true;
		NeedPrimalSpiritsDB.greed = false;
		NeedPrimalSpiritsDB.pass = false;
		print("|cff33ff99NPS|r: Needing all Primal Spirits");
	elseif (msg == "greed") then
		NeedPrimalSpiritsDB.disable = false;
		NeedPrimalSpiritsDB.need = false;
		NeedPrimalSpiritsDB.greed = true;
		NeedPrimalSpiritsDB.pass = false;
		print("|cff33ff99NPS|r: Greeding all Primal Spirits");
	elseif (msg == "pass") then
		NeedPrimalSpiritsDB.disable = false;
		NeedPrimalSpiritsDB.need = false;
		NeedPrimalSpiritsDB.greed = false;
		NeedPrimalSpiritsDB.pass = true;
		print("|cff33ff99NPS|r: Passing on all Primal Spirits");
	else
		print("|cff33ff99Need Primal Spirits|r: Arguments to |cffffff78/nps|r :");
		if NeedPrimalSpiritsDB.disable then
			print("  |cff00ff00Addon is currently disabled");
		elseif NeedPrimalSpiritsDB.need then
			print("  |cff00ff00Currently set to Need");
		elseif NeedPrimalSpiritsDB.greed then
			print("  |cff00ff00Currently set to Greed");
		elseif NeedPrimalSpiritsDB.pass then
			print("  |cff00ff00Currently set to Pass");
		end
		print("  |cffffff78 disable|r - Disable addon");
		print("  |cffffff78 need|r - Roll Need on Primal Spirits");
		print("  |cffffff78 greed|r - Roll Greed on Primal Spirits");
		print("  |cffffff78 pass|r - Pass on Primal Spirits");
	end
end

SlashCmdList.NeedPrimalSpirits = function(msg) slashHandler(msg) end;
SLASH_NeedPrimalSpirits1 = "/nps";
SLASH_NeedPrimalSpirits2 = "/needprimalspirits";

NeedPrimalSpirits.frame = CreateFrame("Frame");
NeedPrimalSpirits.frame:RegisterEvent("START_LOOT_ROLL");
NeedPrimalSpirits.frame:SetScript("OnEvent", NeedPrimalSpirits.OnEvent);
